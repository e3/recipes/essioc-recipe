# essioc conda recipe

Home: https://gitlab.esss.lu.se/e3/essioc

Package license: GPLv3

Recipe license: BSD 3-Clause

Summary: Standard support module that should be included in all ESS IOCs
