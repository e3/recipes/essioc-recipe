require essioc

epicsEnvSet("IOCNAME", "foo")
epicsEnvSet("AS_TOP", "/dev/null")
epicsEnvSet("IOCDIR", "foo")
epicsEnvSet("ERRORLOG_SERVER_PORT", 999)
epicsEnvSet("CAPUTLOG_SERVER_PORT", 999)

iocshLoad("$(essioc_DIR)/common_config.iocsh")

iocInit

dbl
